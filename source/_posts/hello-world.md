---
title: 'Week 1: Overview'
tags: [data analysis]
categories:
  - class
  - announcement
author: Kno
date: 2019-09-11
---
![](http://www.conanblog.com/wp-content/uploads/2017/01/Unknown-1024x587.jpeg)

歡迎來到 Preda 2019 (Fall)! 

這是一個資料的世界--各式各樣資料無所不在，而其中也蘊藏了真實乃至虛擬世界的各種訊息。實境虛境的界線早已模糊，先知與謊騙者難以區分；雖有時可見光明，但闇黑塵囂籠罩---這是一個蠻荒的世界！

[Syllabus 教學計畫表](http://faculty.ndhu.edu.tw/~chtsao/edu/19/preda/preda2019.GC__63860.pdf)

## 起點
* About me: 我的角度/偏見
* Prerequisites: Almost surely nontechnical (~~Calculus, Linear Algebra, Probability, Statistics~~). 一些高中數學以及不是很討厭數學，統計的態度就是很好的準備。如果你有個喜歡探索，喜歡接觸新奇/不熟悉的事物的心智，你應該會享受這趟心智輕旅行。
* Scope of the course: 資料/資料分析之 聽 ~~說~~ 讀 ~~寫~~


## 引想問題
* 資料的形式：以往/現在，Excel, 班級成績單/全聯月會員消費紀錄, 手機使用 log
* small n (sample size), large p (number of variables), sparsity, high dimension problem
* Recommender Systems: 
	* Netflx recommendation system: [Netflix Prize](https://www.netflixprize.com/index.html) , [Netflix Research](https://research.netflix.com/research-area/machine-learning)
* 遊戲：
	* LOL 選角分析：[王子軒巴哈姆特貼文](https://m.gamer.com.tw/forum/C.php?bsn=17532&snA=644376)
	* 桌游： [An analysis of board games (Vatvani)](https://dvatvani.github.io/BGG-Analysis-Part-1.html):
	

### Data Analysis: What? Why and How?
資料分析是什麼？為何我該了解它？而它又會如何影響我？