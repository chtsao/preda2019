---
title:  Preda Project 2019
date: 2019-12-11 08:21:59
tags: [final report, sample presentation]
---
![](https://cdnb.artstation.com/p/assets/images/images/014/404/643/large/jakub-rozalski-drak-fairy-returns.jpg?1543853118)
### 期末報告

* 資料書推。推資料書。。。：選書/報告(report , thesis or paper)原則
* 課堂presentation: 一組 **20～25** 分鐘。整合整組內容，而非截斷假分工。 原則上一堂50分鐘課程有2組報告，中間休息3分鐘。
* 書面報告，內容為推介本次報告之主題，非投影片列印。

### 重要日期
* 12/18(三) 開始報告 （報告日期：12/23(), 12/25; 1/6, 1/8）
* 2020 01/08 23:59   書面報告繳交截止。
### 報告順序
依照課堂透過當天同學隨機產生起始值及 [random.org](https://www.random.org/lists/)產生順序如下

* 12/18（三）
	* 交通安全?：廖泓愷, 王博賢, 葉哲宇, 林皓軒。[連結]()
	* 跑跑卡丁車：吳宇坤, 林士玄, 張博智。[連結]()
* 12/23 (一):  13，6， 3， 1。
	* Uber 之興起：劉群益, 姜秉勳,  [連結]()
	* 狼人殺：杜怡馨, 白敏蒨, 吳姿儀. [連結]()
	* 台灣電影票房分析：童政瑋, 尹健璋, 黃紫瑄. [連結]()
	* 股市分析--為什麼很多人賠錢：梁和雋, 王靖翔, 李錦州. [連結]()
* 12/25 (三): 4, 5
	* 陳逸陵, 蔡茗畯, 曾仁楷 [連結]()
	* 謝昕龍, 周郁森, 張軼棠, 林泓廷, 吳振瑋  [連結]()
* 12/30 (一): 7, 10, 11, 9
	* 黃湘洳, 吳昱潔, 黃培毓, 彭郁庭, 吳佩臻  [連結]()
	* 陳彥熹, 盧俊銘 [連結]()
	* 黃宗元, 劉品佑, 吳林晏, 蔡濬宇 [連結]()
	* 郭郁琪 [連結]()
* 1/6  (一): 8
	* 林佳汝, 吳佳萱, 許鴻源, 楊耀鈞  [連結]()
	* 廖明楷?

### 說明
* 準備報告時可以隨時問問自己以下問題
	* 這本書/論文主要關心的問題
	* 聚焦的問題/圖表/統計數字
	* 針對聚焦問題的心得，文獻資訊探索
	* 參考資料與延伸閱讀
* 書面報告內容與結構建議
	* 摘要: 總結本報告內容
	* 介紹: 為何這本書/論文的內容為何重要/為何是你該知道的？它會對你有何幫助/成長？
	* 主要內容
	* 結論/Take-home message
	* 參考資料/延伸閱讀/連結網址
	
#### 連結

* [Story structure – the hidden framework that hangs your story together](https://www.presentation-guru.com/on-structure-the-hidden-framework-that-hangs-your-story-together/)
* [千分之一約等於十分之七的二十次方](https://www.youtube.com/watch?v=1Nub7szsLow)
* [What's Wrong with TED Talks? Benjamin Bratton at TEDxSanDiego 2013](https://youtu.be/Yo5cKRmJaf0)

#### 你可以學得更好, 更開心, 更有效率 
其他科目都可以不及格，只有學習如何學習絕對要學會。在現在這個時代更是如此。但與其說它是一個要修的科目，不如把它想成一種探索未知世界的態度。以下是最近看到一篇寫得相當好而精簡的部落格文。探討學習有不少經典的書籍。可貴的是，這篇文章精要地總結了許多現代關於學習的新理論與角度。以Kno~~一個大學成績不太好，但後來還學得蠻開心，在這領域多年依然樂在其中＋自學一些有的沒有的還能自得其樂的人~~的視角來看, 要是能早點在大學知道這些，應該可以學得更好，更開心也更有效率。我強烈推薦與分享

[The only technique to learn something new](https://boingboing.net/2015/05/11/the-only-technique-to-learn-so.html) ([James Altucher](https://boingboing.net/2016/04/15/how-minimalism-brought-me-free.html) @Boing Boing)

**學會任何新事物的唯一技法**。作者將它分為十個步驟，由愛她開始。文不算短，你可以先瀏覽大標題：1. Love It, 2. Read it. 3. Try it, but not too hard, ... 你沒看錯，由愛她開始。

加映一個相關的 quote
> Wisdom is a love affair with questions. Knowledge is a love affair with answers. 
> -- <cite>Julio Olalla</cite>

##### 延伸閱讀
* [動機，單純的力量](https://www.books.com.tw/products/0010476180) (Daniel Pink). 紅蘿菠與棍子不僅無法驅動稍有創意的行為，甚至有反效果。
* [刻意練習：比天賦更關鍵的學習法](https://www.books.com.tw/products/0010752714?loc=P_asb_006)(Anders Ericsson and  Robert Pool); 這是「一萬個小時」的原始研究，相較於[異數:成功的故事](https://www.books.com.tw/products/0010668300)(Malcolm Gladwell),  Ericsson and Pool 有更深入而完整的討論--練習的時間長度不是一個領域佼佼者成功的原因，主動、專一、而有計畫步驟的刻意練習才是。
* [心流：最優體驗心理學](https://www.books.com.tw/products/0010816703)(Mihaly Csikszentmihalyi)
[![](https://miro.medium.com/max/362/1*ShU0zwZKC1ti_Oc2f86nPA.png)](https://uxdesign.cc/7-steps-to-achieving-flow-in-ux-design-7ef28adb0de2)


	